(in-package :kong)

(defparameter *kong-url* (quri:uri "http://localhost:8001"))

;; Util

(defmethod json:encode-json ((_ (eql :false)) &optional stream)
  (write-string "false" stream))

(defun normalize-content (content)
  (loop for (x . y) in content
     when y collect (cons x y)))

(defmacro def-kong-endpoint (name &key path method args content backoff
                                    decode-response before around after
                                    documentation)
  (flet ((with-backoff (code)
           (if backoff
               `(exponential-backoff:with-exponential-backoff ,backoff
                  ,code)
               code))
         (with-decoded-response (code)
           (if decode-response
               `(json:decode-json-from-string
                 ,code)
               code))
         (with-around (code)
           (if around
               `(funcall (lambda ,@around)
                         (lambda () ,code))
               code)))
    (flet
        ((supplied-arg? (arg)
           (intern (format nil "~A-P" arg))))
      (let ((content-args (loop for arg in content
                             collect (destructuring-bind (name url-name &optional type)
                                         arg
                                       `(,name nil ,(supplied-arg? name)))))
            (result (gensym "RESULT-")))
        `(progn
           (defun ,name (,@args &key ,@content-args)
           ,@(when documentation (list documentation))
           ,(with-around
                `(progn
                   ,@(when before
                       `((funcall (lambda ,@before))))
                   (let ((,result
                          ,(with-backoff
                               `(progn
                                  ,(with-decoded-response
                                       `(dex:request (quri:merge-uris (quri:uri ,path) *kong-url*)
                                                     :method ,method
                                                     :headers '((:content-type . "application/json"))
                                                     :content (let ((content
                                                                     (remove-if #'null
                                                                                (list ,@(loop for arg in content
                                                                                           collect
                                                                                             (destructuring-bind (name url-name &optional type) arg
                                                                                               `(when ,(supplied-arg? name)
                                                                                                  (cons ,url-name ,(if (eql type :boolean)
                                                                                                                       `(or ,name :false)
                                                                                                                       name)))))))))
                                                                (when content
                                                                  (json:encode-json-to-string content))
                                                                )))))))
                     ,(if after
                          `(funcall (lambda ,@after) ,result)
                          result)))))
           (export ',name)
           )))))

;; Status

(def-kong-endpoint kong-status
    :path "/"
    :method :get
    :decode-response t
    :backoff ((:initial-delay-ms 500) :max-retries 3)
    :documentation "Kong service status")

(def-kong-endpoint node-status
    :path "/status"
    :method :get
    :decode-response t
    :backoff ((:initial-delay-ms 500) :max-retries 3)
    :documentation "Kong node status")

(def-kong-endpoint cluster-status
    :path "/cluster"
    :method :get
    :decode-response t
    :backoff ((:initial-delay-ms 500) :max-retries 3)
    :documentation "Cluster status")              

;; API admin

(def-kong-endpoint get-api
    :path (concatenate 'string "/apis/" name-or-id)
    :args (name-or-id)
    :method :get
    :documentation "Retrieve api"
    :decode-response t)

(def-kong-endpoint add-api
    :path "/apis" :method :post
    :content ((name "name")
              (request-host "request_host")
              (request-path "request_path")
              (strip-request-path "strip_request_path" :boolean)
              (preserve-host "preserve_host" :boolean)
              (upstream-url "upstream_url"))
    :backoff ((:initial-delay-ms 500) :max-retries 3)
    :decode-response t
    :documentation "Add a Kong API")

(def-kong-endpoint update-api
    :path (concatenate 'string "/apis/" id)
    :method :patch
    :args (id)
    :content ((name "name")
              (request-host "request_host")
              (request-path "request_path")
              (strip-request-path "strip_request_path" :boolean)
              (preserve-host "preserve_host" :boolean)
              (upstream-url "upstream_url"))
    :backoff ((:initial-delay-ms 500) :max-retries 3)
    :decode-response t
    ;:around ((f) (funcall f))
                                        ;:after ((res) (print res))
    :documentation "Update an API")

(def-kong-endpoint update-or-create-api
    :path "/apis/"
    :method :put
    :content ((id "id")
              (name "name")
              (request-host "request_host")
              (request-path "request_path")
              (strip-request-path "strip_request_path" :boolean)
              (preserve-host "preserve_host" :boolean)
              (upstream-url "upstream_url"))
    :backoff ((:initial-delay-ms 500) :max-retries 3)
    :decode-response t
    ;:around ((f) (funcall f))
                                        ;:after ((res) (print res))
    :documentation "Update or create API")

(def-kong-endpoint delete-api
    :path (concatenate 'string "/apis/" name-or-id)
    :args (name-or-id)
    :method :delete
    :decode-response nil)

(def-kong-endpoint list-apis
    :path "/apis"
    :method :get
    :decode-response t)

;; Consumer admin

(def-kong-endpoint create-consumer
    :path "/consumers"
    :method :post
    :content ((username "username")
              (custom-id "custom_id"))
    :backoff ((:initial-delay-ms 500) :max-retries 3)
    :decode-response t
    :documentation "Create consumer")

(def-kong-endpoint update-consumer
    :path (concatenate 'string "/consumers" username-or-id)
    :args (username-or-id)
    :method :patch
    :content ((username "username")
              (custom-id "custom_id"))
    :backoff ((:initial-delay-ms 500) :max-retries 3)
    :decode-response t
    :documentation "Update consumer")

(def-kong-endpoint update-or-create-consumer
    :path "/consumers"
    :method :put
    :content ((id "id")
              (username "username")
              (custom-id "custom_id"))
    :backoff ((:initial-delay-ms 500) :max-retries 3)
    :decode-response t
    :documentation "Create or update consumer")

(def-kong-endpoint get-consumer
    :path (concatenate 'string "/consumers/" username-or-id)
    :args (username-or-id)
    :method :get
    :backoff ((:initial-delay-ms 500) :max-retries 3)
    :decode-response t
    :documentation "Retrieve consumer")

(def-kong-endpoint list-consumers
    :path "/consumers"
    :method :get
    :backoff ((:initial-delay-ms 500) :max-retries 3)
    :decode-response t
    :documentation "List consumers")

(def-kong-endpoint delete-consumer
    :path (concatenate 'string "/consumers/" username-or-id)
    :args (username-or-id)
    :method :delete
    :backoff ((:initial-delay-ms 500) :max-retries 3)
    :documentation "Delete consumer")

;; Plugins

(def-kong-endpoint get-enabled-plugins
    :path "/plugins/enabled"
    :method :get
    :backoff ((:initial-delay-ms 500) :max-retries 3)
    :decode-response t
    :documentation "Get enabled plugins")

;; basic-auth

(def-kong-endpoint create-basic-auth-credential
    :args (consumer)
    :path (concatenate 'string "/consumers/" consumer "/basic-auth")
    :method :post
    :content ((username "username")
              (password "password"))
    :decode-response t
    :documentation "Create basic auth credential for consumer")

;; key authentication

(def-kong-endpoint create-api-key
    :args (consumer)
    :path (concatenate 'string "/consumers/" consumer "/key-auth")
    :method :post
    :content ((key "key"))
    :decode-response t
    :documentation "Create an api key")


;; jwt

(def-kong-endpoint create-jwt-credential
    :args (consumer)
    :path (concatenate 'string "/consumers/" consumer "/jwt")
    :method :post
    :content ((key "key")
              (secret "secret"))
    :decode-response t
    :documentation "Create a JWT token")

;; hmac

(def-kong-endpoint create-hmac-credential
    :args (consumer)
    :path (concatenate 'string "/consumers/" consumer "/hmac-auth")
    :method :post
    :content ((username "username")
              (secret "secret"))
    :decode-response t
    :documentation "Create HMAC credential")
